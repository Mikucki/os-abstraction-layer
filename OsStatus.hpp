#ifndef OSSTATUS_HPP
#define OSSTATUS_HPP

namespace Osal
{
enum class Status
{
    OK             = -2,
    ERROR          = -1,
    TRUE           = 1,
    FALSE          = 0,
    NOTIMPLEMENTED = 100,
    WOULDBLOCK     = 200,
};
}
#endif // OSSTATUS_HPP

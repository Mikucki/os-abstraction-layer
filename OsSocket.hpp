#ifndef OSSOCKET_HPP
#define OSSOCKET_HPP

namespace Osal
{

enum class SocketStatus
{
    OK,
    CLOSED,
    ERROR,
    WANT_READ,
    WANT_WRITE,
    NOTIMPLEMENTED,
    WOULDBLOCK
};

enum class SocketProtocol
{
    TCP,
    UDP
};

enum class SocketBlocking
{
    YES,
    NO
};
}

#include "OsSocketImpl.hpp" // For system specific declarations.

namespace Osal
{

static const int OSSOCKET_BACKLOG_SIZE = 10;

SocketStatus OsSocketCreate( OsSocketType& socket, SocketProtocol protocol,
                             SocketBlocking blocking );
SocketStatus OsSocketListen( OsSocketType& socket, const uint32_t ipAddress,
                             const uint16_t port );
SocketStatus OsSocketAccept( OsSocketType& listenSocket,
                             OsSocketType& acceptedSocket );
SocketStatus OsSocketConnect( OsSocketType& socket, const uint32_t ipAddress,
                              const uint16_t port );
SocketStatus OsSocketClose( const OsSocketType& socket );

SocketStatus OsSocketSend( const OsSocketType& socket,
                           const uint8_t* const data, size_t& length );
SocketStatus OsSocketReceive( const OsSocketType& socket, uint8_t* const data,
                              size_t& length );
}
#endif // OSSOCKET_HPP

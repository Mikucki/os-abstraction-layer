#ifndef OSSYSTEM_HPP
#define OSSYSTEM_HPP

#include "OsStatus.hpp"     // For OsStatus
#include "OsSystemImpl.hpp" // For system specific declarations.

namespace Osal
{
Status OsInitialize();
Status OsStart();
}
#endif // OSSYSTEM_HPP

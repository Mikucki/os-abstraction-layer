#ifndef OSTIME_HPP
#define OSTIME_HPP

#include "OsStatus.hpp"   // For OsStatus
#include "OsTimeImpl.hpp" // For system specific declarations.
#include <stdint.h>

#include "BspTime.hpp"
namespace Osal
{
static const uint32_t MICROSECONDS_IN_TICK = 1000000 / configTICK_RATE_HZ;

uint32_t OsMicrosecondsToTicks( uint32_t timeUs )
{
    return timeUs / MICROSECONDS_IN_TICK;
}

uint32_t OsTicksToMicroseconds( uint32_t ticks )
{
    return ticks * MICROSECONDS_IN_TICK;
}

inline void OsGetTimeSinceStart( uint32_t& seconds, uint32_t& microseconds )
{
    // TODO: Implement a more accurate target.
    OS_CriticalSection_Enter();
    seconds = GetCurrentSystickCounter() / 1000000U;

    microseconds = GetCurrentSystickCounter() / 1000U;
    // microseconds =
    // (xTaskGetTickCountFromISR()-seconds*configTICK_RATE_HZ)*MICROSECONDS_IN_TICK;
    OS_CriticalSection_Exit();
}

inline OS_Status OsGetTime( uint32_t* pMilliseconds, uint32_t* pSeconds )
{
    ASSERT_OSAL( pMilliseconds != NULL );
    ASSERT_OSAL( pSeconds != NULL );

    static uint32_t secondsCount                      = 0;
    static TickType_t tickCountAtPreviousSecondSwitch = 0;
    TickType_t tickCountCurrent                       = xTaskGetTickCount();
    TickType_t ticksSinceLastSecondSwitch             = 0;

    if ( tickCountCurrent < tickCountAtPreviousSecondSwitch )
    {
        ticksSinceLastSecondSwitch =
          ( portMAX_DELAY - tickCountAtPreviousSecondSwitch ) +
          tickCountCurrent;
    }
    else
    {
        ticksSinceLastSecondSwitch =
          tickCountCurrent - tickCountAtPreviousSecondSwitch;
    }

    // If the time passed since last time seconds value was increased is greater
    // than 1s, recalculate
    if ( ticksSinceLastSecondSwitch >= configTICK_RATE_HZ )
    {
        secondsCount += ticksSinceLastSecondSwitch / configTICK_RATE_HZ;

        // Check if we can just add ticks to the prev second switch
        if ( ( portMAX_DELAY - tickCountAtPreviousSecondSwitch ) >=
             ( ( ticksSinceLastSecondSwitch / configTICK_RATE_HZ ) *
               configTICK_RATE_HZ ) )
        {
            // Yes, just add the full-second ticks.
            tickCountAtPreviousSecondSwitch +=
              ( ticksSinceLastSecondSwitch -
                ( ticksSinceLastSecondSwitch % configTICK_RATE_HZ ) );
        }
        else
        {
            // No, we have to find the value of overflow and set the count.
            tickCountAtPreviousSecondSwitch =
              ( ticksSinceLastSecondSwitch -
                ( portMAX_DELAY - tickCountAtPreviousSecondSwitch ) ) -
              1;
        }

        // Reduce ticks below 1s.
        ticksSinceLastSecondSwitch %= configTICK_RATE_HZ;
    }

    *pSeconds      = secondsCount;
    *pMilliseconds = ( ticksSinceLastSecondSwitch * 1000 ) / configTICK_RATE_HZ;

    return OS_Status::OSAL_TRUE;
}
}
#endif // OSTIME_HPP

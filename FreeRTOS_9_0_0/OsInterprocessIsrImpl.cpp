#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsInterprocessIsr.hpp"
#include "OsStatus.hpp"
#include <cstdint>

namespace Osal
{
//------------------------------------------------------------------------------
// Semaphore
//------------------------------------------------------------------------------
Status OsSemaphoreNonBlockingTakeIsr( OsSemaphoreType& semaphore )
{
    bool ret = xSemaphoreTakeFromISR( rSemaphore, 0 );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsSemaphoreNonBlockingGiveIsr( OsSemaphoreType& semaphore )
{
    bool ret = xSemaphoreGiveFromISR( rSemaphore, 0 );

    if ( ret == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

//------------------------------------------------------------------------------
// Queue
//------------------------------------------------------------------------------
Status OsQueueReceiveNonBlockingIsr( OsQueueType& rQueue,
                                     void* const pItemBuffer )
{
    BaseType_t ret = 0;
    if ( xQueueReceiveFromISR( rQueue, pItemBuffer, &ret ) == pdTRUE )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsQueueNonBlockingSendIsr( OsQueueType& rQueue,
                                  const void* const pItemToSend )
{
    BaseType_t ret = pdFALSE;
    if ( xQueueSendToBackFromISR( rQueue, pItemToSend, &ret ) == pdPASS )
    {
        return Status::OK;
    }
    else
    {
        return Status::WOULDBLOCK;
    }
}

Status OsQueueIsEmptyIsr( const OsQueueType& rQueue )
{
    if ( xQueueIsQueueEmptyFromISR( rQueue ) == pdTRUE )
    {
        return Status::TRUE;
    }
    else
    {
        return Status::FALSE;
    }
}

Status OsQueueIsFullIsr( const OsQueueType& rQueue )
{
    if ( xQueueIsQueueFullFromISR( rQueue ) == pdTRUE )
    {
        return Status::TRUE;
    }
    else
    {
        return Status::FALSE;
    }
}
}

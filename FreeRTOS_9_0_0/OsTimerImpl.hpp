#ifndef FREERTOS_OSTIMERIMPL_HPP
#define FREERTOS_OSTIMERIMPL_HPP

#include "FreeRTOS.h"

#include "timers.h"

namespace Osal
{
typedef TimerHandle_t OsTimerType;

void OsTimerWrapper( TimerHandle_t pTimerArg );
}

#endif // FREERTOS_OSTIMERIMPL_HPP

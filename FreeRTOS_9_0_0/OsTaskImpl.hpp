#ifndef FREERTOS_OSTASKIMPL_HPP
#define FREERTOS_OSTASKIMPL_HPP

#include "FreeRTOS.h"

#include "task.h"
#include <cstdint>

namespace Osal
{
// Types
typedef TaskHandle_t OsTaskType;

struct OsTaskWrapperParameters
{
    OsTaskFunction pFunction;
    void* pParameters;
};

void* OsTaskFunctionWrapper( void* pParameter );
}
#endif // FREERTOS_OSTASKIMPL_HPP

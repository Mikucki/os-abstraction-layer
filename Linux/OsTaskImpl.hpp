#ifndef LINUX_OSTASKIMPL_HPP
#define LINUX_OSTASKIMPL_HPP

#include "pthread.h"
#include <cstdint>

namespace Osal
{
// Types
typedef ::pthread_t OsTaskType;

struct OsTaskWrapperParameters
{
    OsTaskFunction pFunction;
    void* pParameters;
};

void* OsTaskFunctionWrapper( void* pParameter );
}
#endif // LINUX_OSTASKIMPL_HPP

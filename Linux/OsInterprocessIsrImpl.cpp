#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsInterprocessIsr.hpp"
#include "OsStatus.hpp"
#include <cstdint>

namespace Osal
{
//------------------------------------------------------------------------------
// Semaphore
//------------------------------------------------------------------------------
Status OsSemaphoreNonBlockingTakeIsr( OsSemaphoreType& semaphore )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsSemaphoreNonBlockingGiveIsr( OsSemaphoreType& semaphore )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

//------------------------------------------------------------------------------
// Queue
//------------------------------------------------------------------------------
Status OsQueueReceiveNonBlockingIsr( OsQueueType& rQueue,
                                     void* const pItemBuffer )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsQueueNonBlockingSendIsr( OsQueueType& rQueue,
                                  const void* const pItemToSend )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsQueueIsEmptyIsr( const OsQueueType& rQueue )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsQueueIsFullIsr( const OsQueueType& rQueue )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}
}

#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsStatus.hpp"
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <errno.h>

//------------------------------------------------------------------------------
// Mutex
//------------------------------------------------------------------------------
namespace Osal
{
// Create mutex. Return OsOK if success
Status OsMutexCreate( OsMutexType& mutex )
{
    Status ret = Status::OK;

    if ( pthread_mutex_init( &mutex, NULL ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsMutexDelete( OsMutexType& mutex )
{
    Status ret = Status::OK;

    if ( pthread_mutex_destroy( &mutex ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsMutexLock( OsMutexType& mutex )
{
    Status ret = Status::OK;

    if ( pthread_mutex_lock( &mutex ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsMutexUnlock( OsMutexType& mutex )
{
    Status ret = Status::OK;

    if ( pthread_mutex_unlock( &mutex ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

//------------------------------------------------------------------------------
// Semaphore
//------------------------------------------------------------------------------
static const int SEMAPHORE_SHARE_BETWEEN_PROCESSES    = 1;
static const int SEMAPHORE_SHARE_BETWEEN_THREADS_ONLY = 0;
static const unsigned int SEMAPHORE_INIT_VALUE        = 0;

Status OsSemaphoreCreate( OsSemaphoreType& semaphore )
{
    Status ret = Status::OK;

    if ( sem_init( &semaphore, SEMAPHORE_SHARE_BETWEEN_THREADS_ONLY,
                   SEMAPHORE_INIT_VALUE ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsSemaphoreDelete( OsSemaphoreType& semaphore )
{
    Status ret = Status::OK;

    if ( sem_destroy( &semaphore ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsSemaphoreBlockingTake( OsSemaphoreType& semaphore )
{
    Status ret = Status::OK;

    if ( sem_wait( &semaphore ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsSemaphoreNonBlockingTake( OsSemaphoreType& semaphore )
{
    Status ret = Status::OK;
    int semRet = sem_trywait( &semaphore );

    if ( semRet == -1 && errno == EINTR )
    {
        ret = Status::WOULDBLOCK;
    }
    else if ( semRet == 0 )
    {
        ret = Status::OK;
    }
    else
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

Status OsSemaphoreNonBlockingGive( OsSemaphoreType& semaphore )
{
    Status ret = Status::OK;

    if ( sem_post( &semaphore ) != 0 )
    {
        ret = Status::ERROR;
        OsAssertDbg( false );
    }

    return ret;
}

//------------------------------------------------------------------------------
// Critical Section
//------------------------------------------------------------------------------

Status OsCriticalSectionEnter()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsCriticalSectionExit()
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

//------------------------------------------------------------------------------
// Queue
//------------------------------------------------------------------------------
static const unsigned int QUEUE_SEMAPHORE_INIT_VALUE = 0;

static Status OsQueuePutItem( OsQueueType& rQueue,
                              const void* const pItemToSend );
static Status OsQueueGetItem( OsQueueType& rQueue, void* const pItemBuffer );

Status OsQueueCreate( OsQueueType& rQueue, size_t queueLength,
                      size_t elementSize )
{
    // First set the inialized to false. Used for debugging.
    rQueue.isQueueInitialized = false;

    // Initialize lock first. Don't use it yet. QueueCreate should be called in
    // one thread only on a given queue, before using it.
    if ( pthread_mutex_init( &rQueue.queueLock, NULL ) != 0 )
    {
        //  Nothing to cleanup. Just return error or assert (in dbg).
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Semaphore is used to notify other threads when new data is available in
    // the queue.
    if ( sem_init( &rQueue.notificationSemaphoreFull,
                   SEMAPHORE_SHARE_BETWEEN_PROCESSES,
                   QUEUE_SEMAPHORE_INIT_VALUE ) != 0 )
    {
        // Cleanup mutex, if it fails something is terribly wrong.
        OsAssertDbg( pthread_mutex_destroy( &rQueue.queueLock ) == 0 );
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Semaphore is used to notify other threads when empty possition is in the
    // queue.
    if ( sem_init( &rQueue.notificationSemaphoreEmpty,
                   SEMAPHORE_SHARE_BETWEEN_PROCESSES,
                   QUEUE_SEMAPHORE_INIT_VALUE ) != 0 )
    {
        // Cleanup mutex and full semaphore, if it fails something is terribly
        // wrong.
        OsAssertDbg( pthread_mutex_destroy( &rQueue.queueLock ) == 0 );
        OsAssertDbg( sem_destroy( &rQueue.notificationSemaphoreFull ) == 0 );
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Set empty semaphore to the initial value of queueLength.
    for ( size_t i = 0; i < queueLength; ++i )
    {
        OsAssertDbg( sem_post( &rQueue.notificationSemaphoreEmpty ) != 0 );
    }

    // Set the size of elements and queue length.
    rQueue.numberOfElementsMax     = queueLength;
    rQueue.numberOfElementsCurrent = 0;
    rQueue.sizeOfElement           = elementSize;

    // Set empty/full indexes for fifo.
    rQueue.firstEmpty = 0;
    rQueue.firstFull  = 0;

    // No threads should be currently waiting on notifications.
    rQueue.threadsWaitingOnSemaphore = 0;

    // Allocate memory for keeping elements.
    rQueue.pElementsArray =
      reinterpret_cast<uint8_t*>( std::malloc( queueLength * elementSize ) );

    // Make sure memory was allocated.
    if ( rQueue.pElementsArray == NULL )
    {
        // Cleanup mutex and semaphore. Make sure it destroyed correctly.
        OsAssertDbg( pthread_mutex_destroy( &rQueue.queueLock ) == 0 );
        OsAssertDbg( sem_destroy( &rQueue.notificationSemaphoreFull ) == 0 );
        OsAssertDbg( sem_destroy( &rQueue.notificationSemaphoreEmpty ) == 0 );

        // Return for consistecy. Should never be reached.
        return Status::ERROR;
    }

    rQueue.isQueueInitialized = true;

    return Status::OK;
}

Status OsQueueDelete( OsQueueType& rQueue )
{
    OsAssert( rQueue.isQueueInitialized == true );
    rQueue.isQueueInitialized = false;

    // Let's lock our queue first.
    OsAssertDbg( pthread_mutex_lock( &rQueue.queueLock ) != 0 );

    // Check that there is no other thread waiting on the queue.
    if ( rQueue.threadsWaitingOnSemaphore != 0 )
    {
        // Exit with error.
        OsAssertDbg( pthread_mutex_unlock( &rQueue.queueLock ) != 0 );
        return Status::ERROR;
    }

    // Cleanup full semaphore. Make sure it destroyed correctly.
    OsAssertDbg( sem_destroy( &rQueue.notificationSemaphoreFull ) != 0 );

    // Cleanup empty semaphore. Make sure it destroyed correctly.
    OsAssertDbg( sem_destroy( &rQueue.notificationSemaphoreEmpty ) != 0 );

    // Reset the size of elements and queue length.
    rQueue.numberOfElementsMax     = 0;
    rQueue.numberOfElementsCurrent = 0;
    rQueue.sizeOfElement           = 0;

    // Free the memory for messages.
    free( rQueue.pElementsArray );

    OsAssertDbg( pthread_mutex_unlock( &rQueue.queueLock ) != 0 );

    return Status::OK;
}

Status OsQueueReceiveNonBlocking( OsQueueType& rQueue, void* const pItemBuffer )
{
    OsAssert( rQueue.isQueueInitialized == true );

    // Check the semaphore with nonblocking wait. Errno is set to EAGAIN if
    // semahore is not available.
    int semRet = sem_trywait( &rQueue.notificationSemaphoreFull );

    if ( semRet == -1 && errno == EAGAIN )
    {
        return Status::WOULDBLOCK;
    }
    else if ( semRet == 0 )
    {
        // Let's lock the queue, so we can change it.
        OsAssertDbg( pthread_mutex_lock( &rQueue.queueLock ) != 0 );

        // Put item into the queue buffer.
        OsAssert( OsQueueGetItem( rQueue, pItemBuffer ) == Status::OK );

        // Let's unlock the queue.
        OsAssertDbg( pthread_mutex_unlock( &rQueue.queueLock ) != 0 );

        // Notify everybody that there is an item in the queue.
        OsAssertDbg( sem_post( &rQueue.notificationSemaphoreEmpty ) != 0 );

        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsQueueReceiveBlocking( OsQueueType& rQueue, void* const pItemBuffer )
{
    OsAssert( rQueue.isQueueInitialized == true );

    // Let's wait for Full semaphore notification.
    if ( sem_wait( &rQueue.notificationSemaphoreFull ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Data is in the queue. Let's lock it.
    if ( pthread_mutex_lock( &rQueue.queueLock ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Get item from the queue buffer.
    OsAssert( OsQueueGetItem( rQueue, pItemBuffer ) == Status::OK );

    // Unlock the queue.
    if ( pthread_mutex_unlock( &rQueue.queueLock ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Let's notify any thread waiting for an empty possition in the queue.
    if ( sem_post( &rQueue.notificationSemaphoreEmpty ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    return Status::OK;
}

Status OsQueueSendNonBlocking( OsQueueType& rQueue,
                               const void* const pItemToSend )
{
    OsAssert( rQueue.isQueueInitialized == true );

    // Check the semaphore with nonblocking wait. Errno is set to EAGAIN if
    // semahore is not available.
    int semRet = sem_trywait( &rQueue.notificationSemaphoreEmpty );

    if ( semRet == -1 && errno == EAGAIN )
    {
        return Status::WOULDBLOCK;
    }
    else if ( semRet == 0 )
    {
        // Let's lock the queue, so we can change it.
        OsAssertDbg( pthread_mutex_lock( &rQueue.queueLock ) != 0 );

        // Put item into the queue buffer.
        OsAssert( OsQueuePutItem( rQueue, pItemToSend ) == Status::OK );

        // Let's unlock the queue.
        OsAssertDbg( pthread_mutex_unlock( &rQueue.queueLock ) != 0 );

        // Notify everybody that there is an item in the queue.
        OsAssertDbg( sem_post( &rQueue.notificationSemaphoreFull ) != 0 );

        return Status::OK;
    }
    else
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }
}

Status OsQueueSendBlocking( OsQueueType& rQueue, const void* const pItemToSend )
{
    OsAssert( rQueue.isQueueInitialized == true );

    // Let's wait for an empty possition semaphore notification.
    if ( sem_wait( &rQueue.notificationSemaphoreEmpty ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Let's lock the queue, so we can change it.
    if ( pthread_mutex_lock( &rQueue.queueLock ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Put item into the queue buffer.
    OsAssert( OsQueuePutItem( rQueue, pItemToSend ) == Status::OK );

    // Unlock the queue.
    if ( pthread_mutex_unlock( &rQueue.queueLock ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    // Let's notify any thread waiting for a full possition in the queue.
    if ( sem_post( &rQueue.notificationSemaphoreFull ) != 0 )
    {
        OsAssertDbg( false );
        return Status::ERROR;
    }

    return Status::OK;
}

Status OsQueueIsEmpty( const OsQueueType& rQueue )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

Status OsQueueIsFull( const OsQueueType& rQueue )
{
    OsAssert( false );
    return Status::NOTIMPLEMENTED;
}

static Status OsQueueGetItem( OsQueueType& rQueue, void* const pItemBuffer )
{
    // Sanity checks. Just to make sure.
    OsAssert( rQueue.numberOfElementsCurrent != 0 );
    OsAssert( rQueue.numberOfElementsCurrent <= rQueue.numberOfElementsMax );

    // Decrement number of current elements, and return the oldest one in the
    // FIFO manner.
    rQueue.numberOfElementsCurrent--;

    // Get the first full index. Increment with wrap around.
    size_t i = rQueue.firstFull++ % rQueue.numberOfElementsMax;

    // Copy message to output buffer and make sure it went ok.
    void* copyResult = std::memcpy( pItemBuffer, rQueue.pElementsArray +
                                                   ( rQueue.sizeOfElement * i ),
                                    rQueue.sizeOfElement );
    OsAssert( copyResult != NULL );

    return Status::OK;
}

static Status OsQueuePutItem( OsQueueType& rQueue,
                              const void* const pItemToSend )
{
    // Queue should have at least one empty possition.
    OsAssert( rQueue.numberOfElementsCurrent <= rQueue.numberOfElementsMax );

    // Increment number of current elements, and add a message as a newest
    // element.
    rQueue.numberOfElementsCurrent++;

    // Get the first empty index. Increment with wrap around.
    size_t i = rQueue.firstEmpty++ % rQueue.numberOfElementsMax;

    // Copy message from the input buffer and make sure it went ok.
    void* copyResult =
      std::memcpy( rQueue.pElementsArray + ( rQueue.sizeOfElement * i ),
                   pItemToSend, rQueue.sizeOfElement );
    OsAssert( copyResult != NULL );

    return Status::OK;
}
}

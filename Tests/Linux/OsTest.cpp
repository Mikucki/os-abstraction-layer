#include "OsAssert.hpp"
#include "OsInterprocess.hpp"
#include "OsTask.hpp"
#include <iostream>

int threadsTest();
int queueTest();
int queueMixedTest();
void threadFunctionQueueSend( void* parameters );
void threadFunctionQueueNonblockingSend( void* parameters );
void multithreadedTest();
void multithreadedTestTask( void* pParameter );

void threadFunction( void* parameters );

struct WorkItem
{
    Osal::OsMutexType* pMutex;
    Osal::OsSemaphoreType* pSemaphoreEnd;
    uint32_t* pCounter;
    uint32_t id;
};
int main()
{
    threadsTest();
    queueTest();
    queueMixedTest();
    multithreadedTest();
}

int threadsTest()
{
    Osal::OsMutexType mutex;
    Osal::OsSemaphoreType semaphore;
    uint32_t counter = 0;

    std::cout << "Test start." << std::endl;

    Osal::OsAssert( Osal::OsMutexCreate( mutex ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsSemaphoreCreate( semaphore ) == Osal::Status::OK );

    std::cout << "Prepare task parameters." << std::endl;
    Osal::OsTaskType task1;
    WorkItem item1;
    item1.pCounter      = &counter;
    item1.pMutex        = &mutex;
    item1.id            = 1;
    item1.pSemaphoreEnd = &semaphore;

    Osal::OsTaskType task2;
    WorkItem item2;
    item2.pCounter      = &counter;
    item2.pMutex        = &mutex;
    item2.id            = 2;
    item2.pSemaphoreEnd = &semaphore;

    Osal::OsTaskType task3;
    WorkItem item3;
    item3.pCounter      = &counter;
    item3.pMutex        = &mutex;
    item3.id            = 3;
    item3.pSemaphoreEnd = &semaphore;

    std::cout << "Create tasks." << std::endl;
    Osal::OsAssert( Osal::OsTaskCreate( threadFunction, &item1, 0xFFFF, 10,
                                        "Task1", task1 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskCreate( threadFunction, &item2, 0xFFFF, 10,
                                        "Task2", task2 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskCreate( threadFunction, &item3, 0xFFFF, 10,
                                        "Task3", task3 ) == Osal::Status::OK );

    std::cout << "Wait for semaphores." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 1 taken back." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 2 taken back." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 3 taken back." << std::endl;

    Osal::OsAssert( Osal::OsMutexDelete( mutex ) == Osal::Status::OK );

    std::cout << "All threads finished. Ending program." << std::endl;
    Osal::OsAssert( Osal::OsTaskDelete( task1 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskDelete( task2 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskDelete( task3 ) == Osal::Status::OK );

    return 0;
}

void threadFunction( void* parameters )
{
    WorkItem* work = reinterpret_cast<WorkItem*>( parameters );

    std::cout << "Thread " << work->id << " started. pMutex = " << work->pMutex
              << ". pSemaphoreEnd = " << work->pSemaphoreEnd << "."
              << std::endl;

    while ( true )
    {
        Osal::OsAssert( Osal::OsMutexLock( *work->pMutex ) ==
                        Osal::Status::OK );
        if ( *work->pCounter >= 300 )
        {
            std::cout << "Thread " << work->id << " finished." << std::endl;
            Osal::OsAssert( Osal::OsMutexUnlock( *work->pMutex ) ==
                            Osal::Status::OK );
            Osal::OsAssert( Osal::OsSemaphoreNonBlockingGive(
                              *work->pSemaphoreEnd ) == Osal::Status::OK );

            return;
        }
        else
        {
            ( *( work->pCounter ) )++;
            std::cout << "Thread " << work->id
                      << " incremented counter to: " << *work->pCounter
                      << std::endl;
            Osal::OsAssert( Osal::OsMutexUnlock( *work->pMutex ) ==
                            Osal::Status::OK );
            Osal::OsTaskDelayMs( 100 );
        }
    }
}

const size_t itemsPerTask  = 1000;
const size_t numberOfTasks = 10;
int queueTest()
{
    std::cout << "Start QueueTest." << std::endl;
    Osal::OsQueueType queue;
    Osal::OsTaskType tasks[10];

    std::cout << "Create queue." << std::endl;
    OsQueueCreate( queue, 10, sizeof( uint32_t ) );

    std::cout << "Create tasks." << std::endl;
    for ( size_t i = 0; i < numberOfTasks; i++ )
    {
        Osal::OsAssert( Osal::OsTaskCreate( threadFunctionQueueSend, &queue,
                                            0xFFFF, 10, "Task Send",
                                            tasks[i] ) == Osal::Status::OK );
    }
    std::cout << "Created tasks." << std::endl;

    bool finish                = false;
    uint32_t itemsReceived     = 0;
    uint32_t itemsWithMaxValue = 0;

    while ( finish != true )
    {
        uint32_t itemReceived = 0;

        // std::cout << "Receive blocking." << std::endl;
        Osal::OsAssert( OsQueueReceiveBlocking( queue, &itemReceived ) ==
                        Osal::Status::OK );
        if ( itemReceived == itemsPerTask - 1 )
        {
            itemsWithMaxValue++;
        }
        ++itemsReceived;
        // std::cout << "Received item: " << itemReceived << std::endl;
        // std::cout << "Received items: " << itemsReceived << std::endl;

        if ( itemsReceived == numberOfTasks * itemsPerTask )
        {
            finish = true;
        }
    }

    std::cout << "Finished. Items with max value received: "
              << itemsWithMaxValue << ", expected: " << numberOfTasks
              << std::endl;

    // Closing in 1s
    // Osal::OsAssert( OsTaskDelayMs( 500 ) == Osal::Status::OK );
    for ( size_t i = 0; i < numberOfTasks; i++ )
    {
        Osal::OsTaskDelete( tasks[i] );
    }
    std::cout << "Tasks deleted. Deleting queue." << std::endl;
    OsQueueDelete( queue );

    std::cout << "Queue deleted. Exiting." << std::endl;
    return 0;
}

int queueMixedTest()
{

    std::cout << "Start QueueMixedTest." << std::endl;
    Osal::OsQueueType queue;
    Osal::OsTaskType tasks[10];

    std::cout << "Create queue." << std::endl;
    OsQueueCreate( queue, 10, sizeof( uint32_t ) );

    std::cout << "Create tasks." << std::endl;
    for ( size_t i = 0; i < numberOfTasks; i += 2 )
    {
        Osal::OsAssert( Osal::OsTaskCreate( threadFunctionQueueSend, &queue,
                                            0xFFFF, 10, "Task Send",
                                            tasks[i] ) == Osal::Status::OK );
        Osal::OsAssert( Osal::OsTaskCreate(
                          threadFunctionQueueNonblockingSend, &queue, 0xFFFF,
                          10, "Task Send", tasks[i + 1] ) == Osal::Status::OK );
    }
    std::cout << "Created tasks." << std::endl;

    bool finish                = false;
    uint32_t itemsReceived     = 0;
    uint32_t itemsWithMaxValue = 0;
    uint32_t blockedTimes      = 0;
    while ( finish != true )
    {
        uint32_t itemReceived = 0;
        Osal::Status ret = OsQueueReceiveNonBlocking( queue, &itemReceived );

        // std::cout << "Receive blocking." << std::endl;
        if ( ret == Osal::Status::WOULDBLOCK )
        {
            blockedTimes++;
            Osal::OsTaskDelayMs( 0 );
            continue;
        }
        else if ( ret != Osal::Status::OK )
        {
            Osal::OsAssert( false );
        }

        if ( itemReceived == itemsPerTask - 1 )
        {
            itemsWithMaxValue++;
        }
        ++itemsReceived;
        // std::cout << "Received item: " << itemReceived << std::endl;
        // std::cout << "Received items: " << itemsReceived << std::endl;

        if ( itemsReceived == numberOfTasks * itemsPerTask )
        {
            finish = true;
        }
    }

    std::cout << "Finished. Items with max value received: "
              << itemsWithMaxValue << ", expected: " << numberOfTasks
              << ", wouldblock occurences: " << blockedTimes << std::endl;

    // Closing in 1s
    // Osal::OsAssert( OsTaskDelayMs( 500 ) == Osal::Status::OK );
    for ( size_t i = 0; i < numberOfTasks; i++ )
    {
        Osal::OsTaskDelete( tasks[i] );
    }
    std::cout << "Tasks deleted. Deleting queue." << std::endl;
    OsQueueDelete( queue );

    std::cout << "Queue deleted. Exiting." << std::endl;
    return 0;
}

void threadFunctionQueueSend( void* parameters )
{
    Osal::OsQueueType* pQueue =
      reinterpret_cast<Osal::OsQueueType*>( parameters );

    for ( uint32_t count = 0; count < itemsPerTask; count++ )
    {
        // std::cout << "Send blocking." << std::endl;
        Osal::OsAssert( OsQueueSendBlocking( *pQueue, &count ) ==
                        Osal::Status::OK );
        // Osal::OsAssert( OsTaskDelayMs( 1 ) == Osal::Status::OK );
    }
}

void threadFunctionQueueNonblockingSend( void* parameters )
{
    Osal::OsQueueType* pQueue =
      reinterpret_cast<Osal::OsQueueType*>( parameters );

    for ( uint32_t count = 0; count < itemsPerTask; count++ )
    {
        // std::cout << "Send blocking." << std::endl;
        if ( OsQueueSendNonBlocking( *pQueue, &count ) ==
             Osal::Status::WOULDBLOCK )
        {
            count--;
        }
    }
}

void multithreadedTest()
{
    Osal::OsSemaphoreType semaphore;
    Osal::OsTaskType task1;
    Osal::OsTaskType task2;
    Osal::OsTaskType task3;
    Osal::OsTaskType task4;

    std::cout << "Create a semaphore." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreCreate( semaphore ) == Osal::Status::OK );

    std::cout << "Create tasks." << std::endl;
    Osal::OsAssert( Osal::OsTaskCreate( multithreadedTestTask, &semaphore,
                                        0xFFFF, 10, "Task1",
                                        task1 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskCreate( multithreadedTestTask, &semaphore,
                                        0xFFFF, 10, "Task2",
                                        task2 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskCreate( multithreadedTestTask, &semaphore,
                                        0xFFFF, 10, "Task3",
                                        task3 ) == Osal::Status::OK );
    Osal::OsAssert( Osal::OsTaskCreate( multithreadedTestTask, &semaphore,
                                        0xFFFF, 10, "Task4",
                                        task4 ) == Osal::Status::OK );

    std::cout << "Wait for semaphores." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 1 taken back." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 2 taken back." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 3 taken back." << std::endl;
    Osal::OsAssert( Osal::OsSemaphoreBlockingTake( semaphore ) ==
                    Osal::Status::OK );
    std::cout << "Semaphore 4 taken back." << std::endl;

    std::cout << "Test finished." << std::endl;
}

void multithreadedTestTask( void* pParameter )
{
    Osal::OsSemaphoreType* pSemaphore =
      reinterpret_cast<Osal::OsSemaphoreType*>( pParameter );

    size_t i = 0;
    size_t j = 0;
    for ( i = 0, j = 0;; i++ )
    {
        if ( i == 100000 && j == 100000 )
        {
            break;
        }

        if ( i == 100000 )
        {
            j++;
            i = 0;
        }
    }

    Osal::OsAssert( Osal::OsSemaphoreNonBlockingGive( *pSemaphore ) ==
                    Osal::Status::OK );
}

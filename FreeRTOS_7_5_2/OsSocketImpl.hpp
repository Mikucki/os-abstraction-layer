#ifndef FREERTOS_OSSOCKETIMPL_HPP
#define FREERTOS_OSSOCKETIMPL_HPP

// Include first a specific cc.h file from Lwip, so it's not used from arch in
// gcc.
#include "lwip/arch/cc.h"

//#include "OsSocket.hpp"
// #include "lwip/sockets.h"
// #include "sys/types.h"
#include <cstdint>
#include <cstring>

namespace Osal
{
struct SocketStruct
{
    int fdSocket;
    SocketProtocol protocol;
    SocketBlocking blocking;
};

typedef SocketStruct OsSocketType;
}
#endif // FREERTOS_OSSOCKETIMPL_HPP
